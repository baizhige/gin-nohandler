package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"strconv"
)

func ZeroHandler(c *gin.Context) {
	Zero()
}

func OneHandler(c *gin.Context) {
	var req RequestBody
	err := c.ShouldBindBodyWithJSON(&req)
	if err != nil {
		fmt.Println(err)
		return
	}
	One(req)
}

func OneSimpleHandler(c *gin.Context) {
	value := c.Query("one")
	one, err := strconv.Atoi(value)
	if err != nil {
		fmt.Println(err)
		return
	}
	OneSimple(one)
}

func ThreeHandler(c *gin.Context) {
	value1 := c.Query("one")
	value2 := c.Query("two")
	value3 := c.Query("three")

	one, err := strconv.Atoi(value1)
	if err != nil {
		fmt.Println(err)
		return
	}
	tow := value2
	three, err := strconv.ParseBool(value3)
	if err != nil {
		fmt.Println(err)
		return
	}
	Three(one, tow, three)
}

func FiveHandler(c *gin.Context) {
	value1 := c.Query("one")
	value2 := c.Query("two")
	value3 := c.Query("three")
	value4 := c.Query("four")
	value5 := c.Query("five")
	Five(value1, value2, value3, value4, value5)
}
