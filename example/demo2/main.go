package main

import (
	"gitee.com/baizhige/gin-common-handler/controller"
	"gitee.com/baizhige/gin-common-handler/params"
	"github.com/gin-gonic/gin"
)

var Query = params.Query

// 思路：原生的 与 框架 区别在于反射调用，
//
//		反射调用与正常调用的性能区别主要是看参数数量
//		我们分别测试五组：无参数、有1个基础类型参数、有1个复杂结构体参数、有3个参数、有5个参数
//	 预计： 原生的五组结果都差不多，框架的五组结果随着参数数量耗时增加
//	 实际结果： 框架的五组结果与原生的五组结果 几乎没有差异。 似乎可以得出结论，反射调用方法似乎性能不差。
func main() {
	engine := gin.Default()
	engine.Use(func(ctx *gin.Context) {
		ctx.Next()
		ctx.JSON(200, gin.H{"code": 200, "msg": "ok"})
	})

	// 每组都是20个线程并发请求持续运行5分钟

	originGroup := engine.Group("/origin")
	// 首先是原生组
	//总请求数5062 平均响应时间111  90%响应时间115
	originGroup.POST("/zero", ZeroHandler)
	//总请求数5057 平均响应时间111  90%响应时间115
	originGroup.POST("/one", OneHandler)
	originGroup.POST("/oneSimple", OneSimpleHandler)
	//总请求数5039 平均响应时间110  90%响应时间114
	originGroup.POST("/three", ThreeHandler)
	//总请求数5057 平均响应时间111  90%响应时间115
	originGroup.POST("/five", FiveHandler)

	frameworkGroup := engine.Group("/framework")
	// 然后是框架组
	controller.Select(frameworkGroup)
	//总请求数5066 平均响应时间111  90%响应时间115
	controller.POST("/zero", Zero)
	//总请求数5063 平均响应时间111  90%响应时间115
	controller.POST("/one", One, params.Body[RequestBody]())
	controller.POST("/oneSimple", OneSimple, Query("one", params.INT))
	//总请求数5040 平均响应时间112  90%响应时间113
	controller.POST("/three", Three, Query("one", params.INT), Query("two", params.STRING), Query("three", params.BOOL))
	//总请求数5055 平均响应时间110  90%响应时间115
	controller.POST("/five", Five, Query("one", params.STRING), Query("two", params.STRING), Query("three", params.STRING), Query("four", params.STRING), Query("five", params.STRING))

	engine.Run(":8080")
}
