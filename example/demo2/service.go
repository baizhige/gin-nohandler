package main

import (
	"fmt"
	"time"
)

func Zero() {
	doSomething()
}

func OneSimple(i int) {
	fmt.Sprintln(i)
	doSomething()
}

func One(req RequestBody) {
	fmt.Sprintln(req)
	doSomething()
}

func Three(i int, j string, k bool) {
	fmt.Sprintln(i, j, k)
	doSomething()
}

func Five(a, b, c, d, e string) {
	fmt.Sprintln(a, b, c, d, e)
	doSomething()
}

func doSomething() {
	time.Sleep(100 * time.Millisecond) //睡一百毫秒
}

type RequestBody struct {
	Name       string `json:"name"`
	Age        int    `json:"age"`
	Email      string `json:"email"`
	Addr       string `json:"addr"`
	UserConfig struct {
		Active    bool   `json:"active"`
		CardNo    string `json:"card_no"`
		OtherMark string `json:"other_mark"`
	} `json:"userConfig"`
}
