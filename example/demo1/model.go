package main

import "time"

type CreateUserReq struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type CreateUserResp struct {
	Id         int       `json:"id"`
	Username   string    `json:"username"`
	CreateTime time.Time `json:"createTime"`
}

type UserQueryReq struct {
	Username  string `form:"username"`
	OtherInfo string `form:"otherInfo"`
}
type UserQueryResp struct {
	Id         int       `json:"id"`
	Username   string    `json:"username"`
	CreateTime time.Time `json:"createTime"`
}

type Response struct {
	Code string      `json:"code"`
	Msg  string      `json:"msg"`
	Data interface{} `json:"data"`
}

type PointResp struct {
	Id       int `json:"id"`
	PointNum int `json:"pointNum"`
}
