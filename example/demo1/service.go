package main

import (
	"errors"
	"fmt"
	"math/rand"
	"time"
)

func CreateUser(traceId string, req CreateUserReq) (res CreateUserResp, err error) {
	fmt.Println(traceId, "\treq:%v", req)
	if req.Username == "admin" {
		err = errors.New("cannot create admin")
		return
	}
	res = CreateUserResp{
		Id:         rand.Int(),
		Username:   req.Username,
		CreateTime: time.Now(),
	}
	return
}
func UserQuery(req UserQueryReq) []*UserQueryResp {
	res := make([]*UserQueryResp, 3)
	for i, _ := range res {
		res[i] = &UserQueryResp{
			Id:         i,
			Username:   req.Username + fmt.Sprintf("%v", i),
			CreateTime: time.Now(),
		}
	}
	return res
}

func PointFindById(traceId string, userId int) *PointResp {
	fmt.Println(traceId, "\tuserId:%v", userId)
	return &PointResp{
		Id:       userId,
		PointNum: 99,
	}
}
