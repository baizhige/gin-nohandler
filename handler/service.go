package handler

import (
	"gitee.com/baizhige/gin-common-handler/ctxconst"
	"gitee.com/baizhige/gin-common-handler/params"
	"github.com/gin-gonic/gin"
)

func POST(path string, fun any, params ...params.Param) {
	checkFun(fun)
	var handler = func(ctx *gin.Context) {
		resArr := process(ctx, fun, params)
		ctx.Set(ctxconst.FuncResult, resArr)
	}
	group.POST(path, handler)
}

func GET(path string, fun any, params ...params.Param) {
	checkFun(fun)
	var handler = func(ctx *gin.Context) {
		resArr := process(ctx, fun, params)
		ctx.Set(ctxconst.FuncResult, resArr)
	}
	group.GET(path, handler)
}

func PUT(path string, fun any, params ...params.Param) {
	checkFun(fun)
	var handler = func(ctx *gin.Context) {
		resArr := process(ctx, fun, params)
		ctx.Set(ctxconst.FuncResult, resArr)
	}
	group.PUT(path, handler)
}

func DELETE(path string, fun any, params ...params.Param) {
	checkFun(fun)
	var handler = func(ctx *gin.Context) {
		resArr := process(ctx, fun, params)
		ctx.Set(ctxconst.FuncResult, resArr)
	}
	group.DELETE(path, handler)
}

func Any(path string, fun any, params ...params.Param) {
	// 检查 fun 是否是可调用的
	checkFun(fun)
	var handler = func(ctx *gin.Context) {
		resArr := process(ctx, fun, params)
		ctx.Set(ctxconst.FuncResult, resArr)
	}
	group.Any(path, handler)
}
