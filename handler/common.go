package handler

import (
	"errors"
	"gitee.com/baizhige/gin-common-handler/params"
	"github.com/gin-gonic/gin"
	"reflect"
)

var group gin.IRoutes

var (
	ErrIsNotFunc = errors.New("not func")
)

func Select(routes gin.IRoutes) {
	group = routes
}

func process(ctx *gin.Context, fun any, params []params.Param) (res []interface{}) {
	// 反射获取反射类型对象
	funValue := reflect.ValueOf(fun)

	// 准备参数
	args := make([]reflect.Value, len(params))
	for i, param := range params {
		var arg any
		arg, err := param.GetParam(ctx)
		if err != nil {
			panic(err)
			return
		}
		args[i] = reflect.ValueOf(arg)
	}

	//调用
	responses := funValue.Call(args)
	res = make([]interface{}, len(responses))
	for i, response := range responses {
		res[i] = response.Interface()
	}
	return
}

func checkFun(fun any) {
	// 检查 fun 是否是可调用的
	funValue := reflect.ValueOf(fun)
	if funValue.Kind() != reflect.Func {
		panic(ErrIsNotFunc)
		return
	}
}
