package ctxconst

import "errors"

var TraceIdKey = "traceId"
var ErrTraceIdNotFound = errors.New("traceId not found")
