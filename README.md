# 简介
本框架用于简化Handler/Controller层的数据提取工作。

平时使用Gin开发，都是通过Handler从上下文中获取参数，再把参数传递给Service。
我常常苦恼于参数提取其实一直都在做重复性的工作。

故此有了gin-common-handler来提供通用参数提取解决方案。

# 快速上手

1. 添加依赖
```shell
go get gitee.com/baizhige/gin-common-handler
```

2. 编写Service方法（没错直达Service！！）
```go
func PointFindById(traceId string, userId int) *PointResp {
	fmt.Println(traceId, "\tuserId:%v", userId)
	return &PointResp{
		Id:       userId,
		PointNum: 99,
	}
}
```

3. 注册路由（直接使用Service完成注册
```go
func main() {
	engine := gin.Default()
	engine.Use(Middle) //产生traceId和处理方法结果

	group := engine.Group("/point")                                            
	controller.Select(group)                                                   
	controller.GET("/findById/:id", PointFindById, params.TraceId(), params.Path("id", Int)) 

	engine.Run(":8080")
}


func Middle(ctx *gin.Context) {
fmt.Println("请求进入！")
ctx.Set(ctxconst.TraceIdKey, fmt.Sprintf("%v", rand.Int()))
ctx.Next()
value, exists := ctx.Get(ctxconst.FuncResult)
if !exists {
ctx.JSON(200, Response{
Code: "200",
Msg:  "Success",
Data: nil,
})
}
marshal, _ := json.Marshal(value)
fmt.Println("存在响应:", string(marshal))
funcResults := value.([]interface{})
ctx.JSON(200, Response{"200", "Success", funcResults[0]})
}
```

4. 总结：我们通过直接注册service层的方法，然后在中间件中接收结果即可，service的返回值存放在`ctx.Get(ctxconst.FuncResult)`中

更多参数示例请查阅`example/demo1`.

# 自定义参数
Query、Body、Path、Header等位置的参数是对于网络层面的通用参数，所以框架提供了相关的封装。

但是除此之外还有跟业务相关的参数。例如当前请求的用户。这是非常有用的数据，但是每个项目每个团队之间这个不是很通用，其数据结构固然不会是一样的，因为这与业务强相关。

在框架中有一个`params.Param`接口,我们的参数的处理都依赖于该接口， 可以通过自定义该接口的实现来完成自定义参数。

```go
type AuthUserParam struct{} //代表了授权用户参数种类
func (t *TraceIdParam) GetParam(ctx *gin.Context) (res any, err error) {//实现接口
    res, exists := ctx.Get(ctxconst.AuthUser) //从上下文获取
    if !exists {
    err = errors.New("抛异常啦")
    }
    return
}

func AuthUser() *AuthUserParam { 
    return &AuthUserParam{} 
}
```
很简单对吧 ， 这个玩意就可以直接使用啦：
```go
	controller.GET("/findByUser", findByUser, AuthUser())
```

# 案例介绍
案例位于`example`目录下
* demo1 -- 基本使用案例
* demo2 -- 框架与原生调用方式性能对比