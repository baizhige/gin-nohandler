package params

import (
	"errors"
	"github.com/gin-gonic/gin"
	"strconv"
)

type Param interface {
	GetParam(ctx *gin.Context) (res any, err error)
}

var ErrDataType = errors.New("invalid data type")
var (
	INT    = "INT"
	FLOAT  = "FLOAT"
	STRING = "STRING"
	BOOL   = "BOOL"
)

func strConvert(value string, Type string) (res any, err error) {
	if Type == STRING {
		return value, nil
	}
	if Type == INT {
		res, err = strconv.Atoi(value)
		return
	}
	if Type == FLOAT {
		res, err = strconv.ParseFloat(value, 64)
		return
	}
	if Type == BOOL {
		res, err = strconv.ParseBool(value)
		return
	}
	err = ErrDataType
	return
}
