package params

import "github.com/gin-gonic/gin"

type PathParam[T any] struct {
	Receiver T
}

type PathParamItem struct {
	Key  string
	Type string
}

func (b *PathParamItem) GetParam(ctx *gin.Context) (res any, err error) {
	value := ctx.Param(b.Key)
	return strConvert(value, b.Type)
}

func (p *PathParam[T]) GetParam(ctx *gin.Context) (res any, err error) {
	r := p.Receiver
	err = ctx.ShouldBind(&r)
	res = r
	return
}
