package params

import "github.com/gin-gonic/gin"

type QueryParam[T any] struct {
	Receiver T
}
type QueryParamItem struct {
	Key  string
	Type string
}

func (b *QueryParam[T]) GetParam(ctx *gin.Context) (res any, err error) {
	r := b.Receiver
	err = ctx.ShouldBind(&r)
	res = r
	return
}

func (b *QueryParamItem) GetParam(ctx *gin.Context) (res any, err error) {
	value := ctx.Query(b.Key)
	return strConvert(value, b.Type)
}
