package params

func Body[T any]() *BodyParam[T] {
	var receiver T
	return &BodyParam[T]{Receiver: receiver}
}
func Header(key string) *HeaderParam {
	return &HeaderParam{Key: key}
}

func Query(key, valueType string) *QueryParamItem {
	return &QueryParamItem{Key: key, Type: valueType}
}

func QueryAll[T any]() *QueryParam[T] {
	var t T
	return &QueryParam[T]{Receiver: t}
}

func Path(key, valueType string) *PathParamItem {
	return &PathParamItem{Key: key, Type: valueType}
}
func PathAll[T any]() *PathParam[T] {
	var t T
	return &PathParam[T]{Receiver: t}
}
