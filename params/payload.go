package params

import "github.com/gin-gonic/gin"

type BodyParam[T any] struct {
	Receiver T
}

func (b *BodyParam[T]) GetParam(ctx *gin.Context) (res any, err error) {
	r := b.Receiver
	err = ctx.ShouldBind(&r)
	res = r
	return
}
