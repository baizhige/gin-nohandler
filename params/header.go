package params

import "github.com/gin-gonic/gin"

type HeaderParam struct {
	Key string
}

func (h *HeaderParam) GetParam(ctx *gin.Context) (res any, err error) {
	res = ctx.GetHeader(h.Key)
	return
}
